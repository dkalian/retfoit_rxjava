package com.ndubkov.retrofit_rxjava.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Nickolay on 15.11.2016.
 */

public class Response {
    @SerializedName("city")
    City mCity;
    @SerializedName("cod")
    String mCode;
    @SerializedName("message")
    String mMessage;
    @SerializedName("cnt")
    int mCnt;
    @SerializedName("list")
    List<ListData> mList;

    private class ListData {
        @SerializedName("dt")
        long mDateTime;
        @SerializedName("temp")
        Temp mTemp;
        @SerializedName("pressure")
        float mPressure;
        @SerializedName("humidity")
        float mHumidity;
        @SerializedName("weather")
        List<Weather> mWeatherList;
        @SerializedName("speed")
        float mSpeed;
        @SerializedName("deg")
        float mDeg;
        @SerializedName("clouds")
        float mClouds;
        @SerializedName("rain")
        float mRain;
    }

}
