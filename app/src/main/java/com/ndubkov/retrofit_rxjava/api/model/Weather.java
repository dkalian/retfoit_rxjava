package com.ndubkov.retrofit_rxjava.api.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Nickolay on 15.11.2016.
 */

public class Weather {
    @SerializedName("id")
    int mId;
    @SerializedName("main")
    String mMain;
    @SerializedName("description")
    String mDescription;
    @SerializedName("icon")
    String mIcon;
}
