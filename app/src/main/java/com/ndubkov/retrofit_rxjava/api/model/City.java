package com.ndubkov.retrofit_rxjava.api.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Nickolay on 15.11.2016.
 */

public class City {
    @SerializedName("id")
    String mId;
    @SerializedName("name")
    String mName;
    @SerializedName("coord")
    Coordinates mCoordinates;
    @SerializedName("country")
    String mCountryName;
    @SerializedName("population")
    int mPopulation;
}
