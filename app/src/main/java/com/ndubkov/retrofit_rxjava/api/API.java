package com.ndubkov.retrofit_rxjava.api;

import com.ndubkov.retrofit_rxjava.api.model.Response;

import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import rx.Observable;

/**
 * Created by Nickolay on 15.11.2016.
 */

public interface API {
    @GET("data/2.5/forecast/daily?mode=json&units=metric&cnt=7")
    Observable<Response> getWeatherInCity(@Query("q") String city,
                                          @Query("appid") String appid);
}
