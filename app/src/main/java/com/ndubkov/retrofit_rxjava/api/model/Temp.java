package com.ndubkov.retrofit_rxjava.api.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Nickolay on 15.11.2016.
 */

public class Temp {
    @SerializedName("day")
    float mDay;
    @SerializedName("min")
    float mMin;
    @SerializedName("max")
    float mMax;
    @SerializedName("night")
    float mNight;
    @SerializedName("eve")
    float mEvening;
    @SerializedName("morn")
    float mMorning;
}
