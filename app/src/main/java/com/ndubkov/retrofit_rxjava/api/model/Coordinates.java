package com.ndubkov.retrofit_rxjava.api.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Nickolay on 15.11.2016.
 */

public class Coordinates {
    @SerializedName("lat")
    double mLat;
    @SerializedName("lon")
    double mLon;
}
