package com.ndubkov.retrofit_rxjava.api;

import android.content.Context;
import android.support.annotation.NonNull;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.schedulers.Schedulers;

public class RetrofitProvider {

    @NonNull
    public static Retrofit retrofit(Context context) {
        RxJavaCallAdapterFactory rxAdapter = RxJavaCallAdapterFactory
                .createWithScheduler(Schedulers.io()); // заменить на io в реальном приложении
        return new Retrofit.Builder()
                .baseUrl("http://api.openweathermap.org/")
                .client(createLogClient()) // логгер
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(rxAdapter)
                .build();
    }


    // То самое логирвоание. Для этого нужна dependency в build.gradle
    private static OkHttpClient createLogClient() {
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        clientBuilder.addInterceptor(loggingInterceptor);
        return clientBuilder.build();
    }

    private final static String TAG = RetrofitProvider.class.getSimpleName();
}
