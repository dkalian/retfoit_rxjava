package com.ndubkov.retrofit_rxjava;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.GsonBuilder;
import com.ndubkov.retrofit_rxjava.api.API;
import com.ndubkov.retrofit_rxjava.api.RetrofitProvider;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "ApiTest";
    private static final String APP_ID = "30011cc893b4357a2235a94087854eb4";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        API api = RetrofitProvider.retrofit(this).create(API.class);
        api.getWeatherInCity("London", APP_ID)
                .subscribeOn(Schedulers.newThread()) //  выполняем в новом потоке
                .observeOn(AndroidSchedulers.mainThread()) // результат вернет в UI поток
                .subscribe(response -> {
                    Log.d(TAG, new GsonBuilder().setPrettyPrinting().create().toJson(response));
                });
    }
}
